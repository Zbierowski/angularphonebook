import { Component, Input } from "@angular/core";
import { FormControl } from '@angular/forms';

@Component({
  selector: 'contact-form-error',
  templateUrl: './contact-form-error.html',
  styleUrls: ['./contact-form-error.scss']
})

export class ContactFormError {
  @Input() contactForm: FormControl;
  @Input() controlName: string;
}
