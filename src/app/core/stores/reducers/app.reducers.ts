import { ActionReducerMap } from '@ngrx/store';

import { routerReducer } from '@ngrx/router-store';
import { IAppState } from '../state/app.state';
import { contactReducers } from './contact.reducer';

export const appReducers: ActionReducerMap<IAppState, any> = {
  router: routerReducer,
  contacts: contactReducers,
};