import { Injectable } from "@angular/core";
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';

import { IAppState } from '../state/app.state';
import {
  GetContactSuccess,
  GetContactsSuccess,
  GetContact,
  GetContacts,
  EContactActions,
  AddContact,
  AddContactSuccess,
  EditContact,
  EditContactSuccess
} from '../actions/contact.actions';
import { ContactService } from 'src/app/core/services/contact.service';
import { selectContactList } from '../selectors/contact.selector';
import { IContact } from '../../models/contact.interface';

@Injectable()
export class ContactEffects {

  constructor(private _contactService: ContactService, private _actions$: Actions, private _store: Store<IAppState>) { }

  @Effect()
  getContact$ = this._actions$.pipe(
    ofType<GetContact>(EContactActions.GetContact),
    map(action => action.payload),
    withLatestFrom(this._store.pipe(select(selectContactList))),
    switchMap(([id, contacts]) => {
      const selectedContact = contacts.filter(user => user.id === +id)[0];
      return of(new GetContactSuccess(selectedContact));
    })
  );

  @Effect()
  getContacts$ = this._actions$.pipe(
    ofType<GetContacts>(EContactActions.GetContacts),
    switchMap(() => this._contactService.getContacts()),
    switchMap((contacts: IContact[]) => of(new GetContactsSuccess(contacts)))
  );

  @Effect()
  addContact$ = this._actions$.pipe(
    ofType<AddContact>(EContactActions.AddContact),
    map(action => action.payload),
    withLatestFrom(this._store.pipe(select(selectContactList))),
    switchMap(([contact, contacts]) => this._contactService.addContact(contact)),
    switchMap(([contact, contacts]) => {
      return of(new AddContactSuccess(contacts));
    })
  );

  @Effect()
  editContact$ = this._actions$.pipe(
    ofType<EditContact>(EContactActions.EditContact),
    map(action => action.payload),
    withLatestFrom(this._store.pipe(select(selectContactList))),
    switchMap(([editedContact, contacts]) => this._contactService.editContact(editedContact)),
    switchMap(([editedContact, contacts]) => {
      return of(new EditContactSuccess(contacts));
    })
  );
}