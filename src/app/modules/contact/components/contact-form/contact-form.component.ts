import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';

import { AddContact, GetContacts, EditContact, GetContact } from '../../../../core/stores/actions/contact.actions';
import { IAppState } from '../../../../core/stores/state/app.state';
import { selectSelectedContact } from '../../../../core/stores/selectors/contact.selector';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;
  editedId: number;
  editMode: boolean;
  addMode: boolean;

  constructor(private _route: ActivatedRoute, private _formBuiler: FormBuilder, private _store: Store<IAppState>, private _router: Router) { }

  ngOnInit() {
    this.editedId = +this._route.snapshot.paramMap.get('id');
    this.editMode = this._route.snapshot.paramMap.get('id') !== null;
    this.addMode = this._route.snapshot.paramMap.get('id') === null;

    this.contactForm = this._formBuiler.group({
      name: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern(/^\+48\d{9}$/)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      birthday: ['', Validators.compose([Validators.required, Validators.pattern(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/)])],
      avatar: [''],
    });

    this._store.dispatch(new GetContacts());

    if (this.editMode) {
      this._store.dispatch(new GetContact(this.editedId));
      this._store.pipe(select(selectSelectedContact)).subscribe((selectedContact) => {
        delete selectedContact.id;
        this.contactForm.setValue(selectedContact);
      });
    }
  }

  submitForm(formValue) {
    if (this.addMode) {
      this._store.dispatch(new AddContact(formValue));
    }

    if (this.editMode) {
      formValue.id = this.editedId;
      this._store.dispatch(new EditContact(formValue));
    }

    this._router.navigate(['']);
  }

}
