import { Action } from '@ngrx/store';

import { IContact } from '../../models/contact.interface';

export enum EContactActions {
  GetContacts = '[Contact] Get Contacts',
  GetContactsSuccess = '[Contact] Get Contacts Success',
  GetContact = '[Contact] Get Contact',
  GetContactSuccess = '[Contact] Get Contact Success',
  AddContact = '[Contact] Add Contact',
  AddContactSuccess = '[Contact] Add Contact Success',
  EditContact = '[Contact] Edit Contact',
  EditContactSuccess = '[Contact] Edit Contact Success',
};

export class GetContacts implements Action {
  public readonly type = EContactActions.GetContacts;
}

export class GetContactsSuccess implements Action {
  public readonly type = EContactActions.GetContactsSuccess;
  constructor(public payload: IContact[]) { }
}

export class GetContact implements Action {
  public readonly type = EContactActions.GetContact;
  constructor(public payload: number) { }
}

export class GetContactSuccess implements Action {
  public readonly type = EContactActions.GetContactSuccess;
  constructor(public payload: IContact) { }
}

export class AddContact implements Action {
  public readonly type = EContactActions.AddContact;
  constructor(public payload: IContact) { }
}

export class AddContactSuccess implements Action {
  public readonly type = EContactActions.AddContactSuccess;
  constructor(public payload: IContact[]) { }
}

export class EditContact implements Action {
  public readonly type = EContactActions.EditContact;
  constructor(public payload: IContact) { }
}

export class EditContactSuccess implements Action {
  public readonly type = EContactActions.EditContactSuccess;
  constructor(public payload: IContact[]) { }
}

export type ContactActions = GetContacts | GetContactsSuccess | GetContact | GetContactSuccess | AddContact | AddContactSuccess | EditContact | EditContactSuccess;