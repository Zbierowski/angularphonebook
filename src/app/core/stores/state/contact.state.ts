import { IContact } from '../../models/contact.interface';

export interface IConstactState {
  contacts: IContact[];
  selectedContact: IContact;
}

export const initialContactState: IConstactState = {
  contacts: null,
  selectedContact: null,
};