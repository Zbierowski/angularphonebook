import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../../../core/services/contact.service';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../../../core/stores/state/app.state';
import { selectContactList } from '../../../../core/stores/selectors/contact.selector';
import { GetContacts } from '../../../../core/stores/actions/contact.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  private contacts$ = this._store.pipe(select(selectContactList));

  constructor(private _contactService: ContactService, private _store: Store<IAppState>, private _router: Router) { }

  ngOnInit() {
    this._contactService.setContactsInLocalStorage();
    this._store.dispatch(new GetContacts());
  }

  addContact() {
    this._router.navigate(['contact-form/add']);
  }
}
