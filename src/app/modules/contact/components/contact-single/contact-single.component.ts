import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/core/stores/state/app.state';
import { GetContacts, GetContact } from 'src/app/core/stores/actions/contact.actions';
import { selectSelectedContact } from '../../../../core/stores/selectors/contact.selector';
import { IContact } from 'src/app/core/models/contact.interface';

@Component({
  selector: 'app-contact-single',
  template: '<app-contact [contact]="contact"></app-contact>',
})
export class ContactSingleComponent implements OnInit {
  contactId: number;
  contact: IContact;

  constructor(private _route: ActivatedRoute, private _store: Store<IAppState>, private _router: Router) { }

  ngOnInit() {
    this.contactId = +this._route.snapshot.paramMap.get('id');
    this._store.dispatch(new GetContacts());
    this._store.dispatch(new GetContact(this.contactId));
    this._store.pipe(select(selectSelectedContact)).subscribe((selectedContact) => {
      this.contact = selectedContact;
    });
  }

}
