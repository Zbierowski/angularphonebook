import { Injectable } from '@angular/core';
import { IContact } from '../models/contact.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private contacts: IContact[] = [
    {
      "id": 1,
      "name": "Erin Eyeball",
      "phone": "(123) 456-7890",
      "email": "one.eye.open@ilumin.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/1"
    },
    {
      "id": 2,
      "name": "Johnathan Homebody",
      "phone": "(123) 456-7890",
      "email": "stayathomedad@wheresmom.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/2"
    },
    {
      "id": 3,
      "name": "Cletus Weatherly",
      "phone": "(123) 456-7890",
      "email": "cletus@netscape.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/3"
    },
    {
      "id": 4,
      "name": "Shirley Travels",
      "phone": "(123) 456-7890",
      "email": "shirley.travels@cityscape.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/4"
    },
    {
      "id": 5,
      "name": "John Watcher",
      "phone": "(123) 456-7890",
      "email": "train.photo.junkie@photonow.net",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/5"
    },
    {
      "id": 6,
      "name": "Curly Jenny",
      "phone": "(123) 456-7890",
      "email": "littlewhitebows@aol.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/6"
    },
    {
      "id": 7,
      "name": "Old Man Jenkins",
      "phone": "(123) 456-7890",
      "email": "wiseman@hotmail.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/8"
    },
    {
      "id": 8,
      "name": "Becky",
      "phone": "(123) 456-7890",
      "email": "whatsoverthere@myspace.com",
      "birthday": "01/01/1980",
      "avatar": "http://lorempixel.com/300/300/people/9"
    }
  ];

  setContactsInLocalStorage() {
    if (localStorage.getItem('contacts')) {
      const retrievedContacts = JSON.parse(localStorage.getItem('contacts'));
      this.contacts = retrievedContacts;
    } else {
      this.updateLocalStorage(this.contacts);
    }
  }

  getContacts() {
    return Observable.create(observer => {
      observer.next(this.contacts);
      observer.complete();
    });
  }

  addContact(contact: IContact) {
    return Observable.create(observer => {
      const retrievedContacts = JSON.parse(localStorage.getItem('contacts'));
      contact.id = this.createNextIndex(retrievedContacts);
      retrievedContacts.push(contact);
      this.updateLocalStorage(retrievedContacts);
      observer.next(retrievedContacts);
      observer.complete();
    });
  }

  editContact(editedContact: IContact) {
    return Observable.create(observer => {
      const retrievedContacts = JSON.parse(localStorage.getItem('contacts'));
      const foundContactIndex = retrievedContacts.findIndex(contact => contact.id === editedContact.id);
      retrievedContacts[foundContactIndex] = editedContact;
      this.updateLocalStorage(retrievedContacts);
      observer.next(retrievedContacts);
      observer.complete();
    });
  }

  updateLocalStorage(contacts: IContact[]) {
    const contactsToUpdate = JSON.stringify(contacts);
    localStorage.setItem('contacts', contactsToUpdate);
  }

  createNextIndex(retrievedContacts) {
    let lastId = retrievedContacts[retrievedContacts.length - 1].id;
    lastId++;
    return lastId;
  }
}
