import { Component, Input } from '@angular/core';
import { IContact } from 'src/app/core/models/contact.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {
  @Input() contact: IContact;

  constructor(private _router: Router) { }

  editContact(id: number) {
    this._router.navigate(['contact-form/edit', id]);
  }
}
