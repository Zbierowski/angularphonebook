import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './components/contact/contact.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactSingleComponent } from './components/contact-single/contact-single.component';
import { ContactFormError } from './components/contact-form/contact-form-error';

@NgModule({
  declarations: [
    ContactComponent,
    ContactListComponent,
    ContactFormComponent,
    ContactSingleComponent,
    ContactFormError,
  ],
  imports: [
    CommonModule,
    ContactRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ContactModule { }
