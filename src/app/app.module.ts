import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactService } from './core/services/contact.service';
import { StoreModule } from '@ngrx/store';
import { appReducers } from './core/stores/reducers/app.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ContactEffects } from './core/stores/effects/contact.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ContactModule } from './modules/contact/contact.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([ContactEffects]),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    ContactModule,
    SharedModule,
  ],
  providers: [ContactService],
  bootstrap: [AppComponent],
})
export class AppModule { }
