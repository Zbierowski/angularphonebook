import { EContactActions } from "../actions/contact.actions";
import { ContactActions } from '../actions/contact.actions';
import { initialContactState, IConstactState } from '../state/contact.state';

export const contactReducers = (state = initialContactState, action: ContactActions): IConstactState => {
  switch (action.type) {
    case EContactActions.GetContactsSuccess: {
      return {
        ...state,
        contacts: action.payload,
      }
    }

    case EContactActions.GetContactSuccess: {
      return {
        ...state,
        selectedContact: action.payload,
      }
    }

    case EContactActions.AddContactSuccess: {
      return {
        ...state,
        contacts: action.payload
      }
    }

    case EContactActions.EditContactSuccess: {
      return {
        ...state,
        contacts: action.payload
      }
    }

    default:
      return state;
  }
};
