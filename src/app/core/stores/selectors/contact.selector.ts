import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { IConstactState } from '../state/contact.state';

const selectContacts = (state: IAppState) => state.contacts;

export const selectContactList = createSelector(
  selectContacts,
  (state: IConstactState) => state.contacts
);

export const selectSelectedContact = createSelector(
  selectContacts,
  (state: IConstactState) => state.selectedContact
);
