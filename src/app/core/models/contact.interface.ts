export interface IContact {
  id: number;
  name: string;
  phone: string;
  email: string;
  birthday: string;
  avatar: string;
};