import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactSingleComponent } from './components/contact-single/contact-single.component';

const routes: Routes = [
  {
    path: '',
    component: ContactListComponent,
  },
  {
    path: 'contact/:id',
    component: ContactSingleComponent
  },
  {
    path: 'contact-form/add',
    component: ContactFormComponent
  },
  {
    path: 'contact-form/edit/:id',
    component: ContactFormComponent
  }
];

@NgModule({

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
