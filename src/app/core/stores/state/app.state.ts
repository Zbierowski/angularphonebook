import { RouterReducerState } from '@ngrx/router-store';

import { IConstactState, initialContactState } from './contact.state';

export interface IAppState {
  router?: RouterReducerState;
  contacts: IConstactState;
};

export const initialAppState: IAppState = {
  contacts: initialContactState,
};

export function getInitialState(): IAppState {
  return initialAppState;
};